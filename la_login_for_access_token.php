<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request){
        $login = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (!Auth::attempt($login)){
            return response(['message' => 'Неверные логин и пароль']);
        }

        $token = Auth::user()->createToken('authToken')->accessToken;
        return response([
           'user' => Auth::user(),
           'access_token' => $token
        ]);
    }
}
