<?php
/*
Вывести элементы массива с конца, используя рекурсию
*/
function f1($ar){
	$first_element = array_shift($ar); // извлекаем первый элемент массива
	if (count($ar) > 0){
		f1($ar);
	}
	echo $first_element.' ';
}

f1 ([4, 6 , 8, 10]);
